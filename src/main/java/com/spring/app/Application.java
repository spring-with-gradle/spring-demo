package com.spring.app;

import com.spring.app.model.AppProperties;
import com.spring.app.process.ProcessStatus;
import com.spring.app.process.ProcessInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@Slf4j
@SpringBootApplication
public class Application implements ApplicationRunner {

    @Autowired
    private AppProperties properties;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ProcessStatus processStatus;

    @Autowired
    private ProcessInfo processInfo;

    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
        }catch(Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info(properties.getProcessName()+" Process started....");
        processStatus.start();
        processStatus.running();
        log.info("LOG PATH : "+System.getProperty("LOG_PATH"));
        log.info("LOG FILE : "+System.getProperty("LOG_FILE"));
        log.info("Updated v1.0.1");
        processStatus.end();
        processInfo.getProcessStatus();
        SpringApplication.exit(context);

        log.info("**************************************");
        log.info("****** Process Execution Finish ******");
        log.info("**************************************");
    }



}
