package com.spring.app.process;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProcessInfo {


    public void getProcessStatus(){

      log.info("Spring boot process running...");

    }
}
