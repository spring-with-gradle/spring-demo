package com.spring.app.process;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ProcessStatus {

    public void start(){

        log.info("Process is start...");

    }

    public void running(){

        log.info("Process is running...");

    }

    public void end(){

        log.info("Process is running...");

    }

}
